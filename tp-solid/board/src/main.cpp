
#include "Board.hpp"
#include "Reportable.hpp"
#include "NumBoard.hpp"

void testBoard(Board & b, Reportable& r) {
    std::cout << b.getTitle() << std::endl;
    b.add("item 1");
    b.add("item 2");
    r.report(b);
    
}

void testNumBoard(NumBoard & b , Reportable& r){
    std::cout << b.getTitle() << std::endl;
    b.numAdd("item");
    b.numAdd("item");
    r.report(b);
}

int main() {

    Board b1;
    NumBoard b2;
    ReportFile report("tmp.txt");
    ReportStdout report2;
    testBoard(b1, report2);
    testNumBoard(b2, report);

    return 0;
}

