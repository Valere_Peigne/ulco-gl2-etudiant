#pragma once
#include "Board.hpp"

class NumBoard : public Board {
    private:
        int _n;

    public:
        NumBoard() : _n(1) {}

        void numAdd(const std::string & t) {
            add(std::to_string(_n) + ". " + t);
            _n++;
        }

        virtual std::string getTitle() const override {
            return "NumBoard";
        }
};