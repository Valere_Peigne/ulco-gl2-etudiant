#pragma once

#include "Itemable.hpp"

class Reportable {
    public:
    virtual ~Reportable() = default;
    virtual void report(const Itemable & itemable) = 0;
};

class ReportFile : public Reportable{

private:
    std::ofstream _ofs;

public:
    ReportFile(const std::string & filename) : _ofs(filename) {}
    void report(const Itemable& itemable) override {
        for (const std::string & item : itemable.getItems())
            _ofs << item << std::endl;
        _ofs << std::endl;
    }

};

class ReportStdout : public Reportable{

public:
    void report(const Itemable& itemable) override {
        for (const std::string & item : itemable.getItems())
            std::cout << item << std::endl;
        std::cout << std::endl;
    }
};