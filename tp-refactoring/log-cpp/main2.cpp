#include <fstream>
#include <functional>
#include <iostream>

using logFunc_t = std::function<void(const std::string &)>;

int add3(int n) {
    return n+3;
}

int mul2(int n) {
    return n*2;
}

int mycompute(logFunc_t logFunction, int v0) {
    logFunction("add3 " + std::to_string(v0) );
    const int v1 = add3(v0);
    logFunction("mul2 " + std::to_string(v1) );
    const int v2 = add3(v1);
    return v2;
}



int main() {
    std::cout << "this is log-cpp" << std::endl;

    std::ofstream ofs("log.txt");

    logFunc_t logCout = [](const std::string & message) { std::cout << message << std::endl; };
    int resultat = mycompute(logCout, 18);

    logFunc_t logFile = [&ofs](const std::string & message) { ofs << message << std::endl; };
    int resultat2 = mycompute(logFile, 18);

    return 0;
}

