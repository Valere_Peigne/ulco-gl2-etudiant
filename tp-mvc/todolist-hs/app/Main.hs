import Board
import View

-- import Text.Read (readMaybe)

-- Le main représente la partie controller

loop :: Board -> IO ()
loop b = do
    putStrLn ""
    printBoard b
    input <- getLine
    let (action:tache) = words input
    case action of
        "add" -> do 
            let (_, b') = addTodo (unwords tache) b
            loop b'
        "done" -> do 
            putStrLn input
            loop b
        "quit" -> putStrLn "Programme en cours d'arrêt..."
        otherwise -> putStrLn "ERROR"

    -- TODO

main :: IO ()
main = loop newBoard

