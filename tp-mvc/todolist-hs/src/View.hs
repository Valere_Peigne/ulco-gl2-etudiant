module View where
    
import Board
import Task

-- Le fichier view représente la partie View

showTask :: Task -> String
showTask t = show (_taskId t) ++ ". " ++ (_taskName t)

printBoard :: Board -> IO ()
printBoard b = do
    putStrLn "TODO"
    let todoList = _boardTodo b
    mapM_ (putStrLn . showTask) todoList
    putStrLn "DONE"
    let doneList = _boardDone b
    mapM_ (putStrLn . showTask) doneList

