#pragma once

#include "task.hpp"

struct Board{
    int _boardId;
    Task _boardTodo;
    Task _boardDone;

    Board();
    std::pair<int,Board> addTodo(std::string str, Board board);
    Board toDone(int i0, Board board);

};

