#pragma once

#include <string>

struct Task{
    int _taskId;
    std::string _taskName;
};