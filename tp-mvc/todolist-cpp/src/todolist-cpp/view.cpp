#include <todolist-cpp/view.hpp>
#include <todolist-cpp/task.hpp>
#include <todolist-cpp/board.hpp>

#include <iostream>

void showTask(Task task){
    std::cout << task._taskId << ". " << task._taskName << std::endl; 
}

