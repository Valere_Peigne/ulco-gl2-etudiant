#include <todolist-cpp/board.hpp>


Board::Board(){
    _boardId = 1;
}

std::pair<int,Board> Board::addTodo (std::string str, Board board){
    int i = board._boardId;
    board._boardId = i + 1;
    Task task;
    task._taskId = i;
    task._taskName = str;
    board._boardTodo = task;
    return std::make_pair(i, board);
}
