#include <cmath>

double mySin(double a, double x, double b){
    return std::sin(2*M_PI*(a*x + b));
}

#include <emscripten/bind.h>

EMSCRIPTEN_BINDINGS(sinus) {
    emscripten::function("mySin", &mySin);
}
