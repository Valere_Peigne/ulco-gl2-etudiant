with import <nixpkgs> {};

let
  masuperlib-src = fetchTarball "https://gitlab.com/Valere_Peigne/masuperlib/-/archive/v0.1/masuperlib-v0.1.tar.gz";
  masuperlib = callPackage masuperlib-src {};

in clangStdenv.mkDerivation {
    name = "masuperpp";
    src = ./.;
    nativeBuildInputs = [ cmake ];
    buildInputs = [ masuperlib ];
}

